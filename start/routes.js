'use strict'

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Database = use('Database')
const Route = use('Route')

Route.on('/').render('dashboard')

Route.get('/users', 'UserController.index').as('users.index')
Route.get('/users/create', 'UserController.create').as('users.create')
Route.get('/users/:id/edit', 'UserController.edit').as('users.edit')

Route.get('/db', async() => {
  /*const db = await Database.from('posts').insert([{title: 'สวัสดีคะ', detail: 'หนูชื่อเจนมากันนุ่นและมากับโบ'},{title: 'สวัสดีจ้า', detail: 'ผมชื่อโบมากับนุ่นและมากับเจน'}])
  if (db) {
    return 'OK'
  }
  else {
    return 'NOT OK'
  }*/
  const posts = await Database.from('posts')
  return posts
})
